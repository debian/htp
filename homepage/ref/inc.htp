

<BLOCK syntax>
<INC macroname1="increment1" [macroname2="increment2" ... ] [GLOBAL]>
</BLOCK>

<BLOCK synopsis>

The <STRONG>INC</STRONG> can be used for incrementing/decrementing a
variable or for variable addition.  The basic form is shown above.
<P>
The "increment" is anything that evaluates to a integer.  It can be a
literal string or a macro preceeded by the '$'-operator.  The
macroname must be the name of a defined macro that contains an
integer.  If the increment is ommitted it defaults to 1.
The effect of this operation is that the macro is redefined
to contain the sum of its previous value and its increment.
<P>
To increment or decrement a variable you can use this form:
<htpcode>
    <INC i="1" j="-1">
</htpcode>
<p>
Note that INC doesn't check if the strings really denote integers.  It
may give weired results when used on arbitrary strings.  The integer
values are limited to the range -2^31..2^31.
<P>
Note further that htp wasn't designed to do extensive calculations.
The INC tag is useful in conjunction with WHILE to iterate over the
defined blocks.  
</BLOCK>

