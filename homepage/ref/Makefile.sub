#
# makefile - htp
#
# HTML pre-processor
# Copyright (c) 2011 Jochen Hoenicke
#

p$(dir) := $(d)
d := $(dir)

# subdirectories
dir:=$(d)/pic
include $(dir)/Makefile.sub

#################

DEF$(d) := htp.def macros.def toc.def htp.htt
SRC$(d) := alttext.htp block.htp blockdef.htp bugs.htp comment.htp \
  def.htp default.htp expand.htp file.htp history.htp \
  if.htp img.htp imageurl.htp inc.htp index.htp intro.htp macros.htp \
  metatag.htp opt.htp options.htp output.htp quote.htp set.htp template.htp \
  undef.htp unset.htp usage.htp use.htp while.htp wishlist.htp \
  tutorial.htp include.htp license.htp

DEF$(d) := $(DEF$(d):%=$(d)/%)
SRC$(d) := $(SRC$(d):%=$(d)/%)
HTML$(d) := $(SRC$(d):%.htp=%.html)
MISC$(d) := $(d)/hlhtp.pl $(d)/pphtp.pl $(d)/styles.css

HPTUTDIR := $(d)/tut
HPTUTSTAMP := $(d)/tut/.stamp
HPTUT_SRC := $(HPTUTDIR)/tut1.htp $(HPTUTDIR)/tut2.htp
HPTUT_SRC += $(HPTUTDIR)/tut3.htp $(HPTUTDIR)/jh.htp
HPTUT_DEF += $(HPTUTDIR)/footer.hti $(HPTUTDIR)/header.hti
HPTUT_DEF += $(HPTUTDIR)/tut3.htt $(HPTUTDIR)/tut4.htt
HPTUT_HTML := $(HPTUT_SRC:%.htp=%.html)
HTML += $(HTML$(d)) $(HPTUT_HTML)

DISTDIRS += $(d) $(d)/pic
DISTFILES += $(d)/Makefile.sub $(SRC$(d)) $(DEF$(d)) $(MISC$(d))
CLEAN += $(d)/*~ $(d)/htp.rsp $(d)/*.html $(d)/htpdeb.out $(d)/ref.tmp

$(HTML$(d)): $(DEF$(d)) $(HPTUTSTAMP)
$(HPTUT_DEF): $(HTML$(d))
	touch $@
$(HPTUT_SRC): $(HTML$(d))
	touch $@
$(HPTUT_HTML): $(HPTUT_DEF)

all: $(HPREF_HTML) $(HPTUT_HTML)

clean::
	rm -rf $(HPTUTDIR)

$(HPTUTSTAMP):
	mkdir -p $(HPTUTDIR)
	touch $@

.PHONY: clean-ref install-doc rsync-ref rsync-tut

install: install-doc
install-doc:: $(HTML$(d)) $(SRC$(d)) $(DEF$(d)) $(MISC$(d))
	mkdir -p $(pkgdocdir)
	for i in $^; do  \
	$(INSTALL) $$i $(pkgdocdir); done

install-doc:: $(HPTUT_HTML) $(HPTUT_SRC) $(HPTUT_DEF)
	mkdir -p $(pkgdocdir)/tut
	for i in $^; do \
	$(INSTALL) $$i $(pkgdocdir)/tut; done

rsync:: $(HTML$(d)) $(SRC$(d)) $(DEF$(d)) $(MISC$(d))
	chmod a+r $^
	$(RSYNC) $^ $(RSYNCDIR)/ref

rsync:: $(HPTUT_HTML) $(HPTUT_SRC) $(HPTUT_DEF)
	chmod a+r $^
	$(RSYNC) $^ $(RSYNCDIR)/ref/tut

# restore parent dir
d := $(p$(d))
