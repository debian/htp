/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
//
// image-proc.h
//
// specialized markup processors
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef IMAGE_PROC_H
#define IMAGE_PROC_H

#include "defs.h"

uint ImageProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);

uint AltTextProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);

uint ImageUrlProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);

#endif
