/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
//
// htp.h
//
// Common include file
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef HTP_H
#define HTP_H

/*
// for assert()
*/
#ifndef DEBUG
#define NDEBUG
#endif

/*
// common definitions
*/
typedef unsigned int        BOOL;
#define FALSE               (0)
#define TRUE                (1)
#define ERROR               ((uint) -1)
#define NUL					((char) 0)
typedef unsigned char       BYTE;
typedef unsigned short      WORD;
typedef unsigned long       DWORD;

/*
// common data sizes
*/
#define KBYTE               (1024L)
#define MBYTE               (KBYTE * KBYTE)
#define GBYTE               (MBYTE * KBYTE)

/*
// debug information macro
*/
#if DEBUG

#define DEBUG_PRINT(p) \
    DebugMsg("\n(%s) %s line %u: ", PROGRAM_NAME, __FILE__, __LINE__); \
    DebugMsg p; \
    DebugMsg("\n");

#else

#define DEBUG_PRINT(p)

#endif

/*
// macros to handle unused/unreferenced local variables & parameters in
// functions
//
// DEBUG_* macros are defined as nothing in non-debug version, too catch
// unreferenced stuff in final build version
//
// Macros wholesale ripped off from Windows NT device driver kit ... if
// they originated elsewhere, this is the first I've seen them
*/
#define UNREF_PARAM(p)      ((p) = (p))
#define UNREF_LOCAL(l)      ((l) = (l))

#if DEBUG

#define DEBUG_UNREF_PARAM(p)    ((p) = (p))
#define DEBUG_UNREF_LOCAL(l)    ((l) = (l))

#else

#define DEBUG_UNREF_PARAM(p)
#define DEBUG_UNREF_LOCAL(l)

#endif

/*
// common macros
*/
#define MAKE_WORD(h, l)  ((((WORD) (h)) << 8) | ((WORD) (l)))
#define MAKE_DWORD(hh,hl,lh,ll) \
    ((((DWORD) (hh)) << 24) | (((DWORD)(hl)) << 16) \
     | (((DWORD) (lh)) << 8) | ((DWORD)(ll)))

/*
// C library
*/
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <memory.h>
#include <limits.h>

/*
// operating-system dependent header file
*/
#include "os.h"

/*
// modules
*/
#include "image.h"
#include "html.h"
#include "ver.h"
#include "textfile.h"
#include "streams.h"
#include "varstore.h"
#include "msg.h"
#include "suballoc.h"

/*
// returns the markup type flag ... either the closing or opening delimiter
// in the markup can be passed in
*/
uint MarkupType(char delim);

/*
// common functions (maintained in util.c)
*/
BOOL CreateTempFilename(char *tempfilename, uint size);
char *StringCopy(char *dest, const char *src, uint size);
char *ConvertDirDelimiter(const char *pathname);
BOOL FileExists(const char *pathname);
const char *FindFilename(const char *pathname);
char *DuplicateString(const char *src);
char *DuplicateSubString(const char *src, int len);

#ifndef HAVE_STPCPY
char *stpcpy(char *dest, const char *src);
#endif

/*
// re-entrant strtok() functions and structures
*/

typedef struct tagFIND_TOKEN
{
    const char          *tokens;
    char                *lastChar;
    char                *nextStart;
} FIND_TOKEN;

char *StringFirstToken(FIND_TOKEN *findToken, char *string, const char *tokens);
char *StringNextToken(FIND_TOKEN *findToken);

#endif

