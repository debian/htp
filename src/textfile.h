/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
//
// textfile.h
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef TEXTFILE_H
#define TEXTFILE_H

/*
// text file function
*/
typedef struct tagFILEINFO
{
    FILE            *file;
    uint            pendingSpaces;
    BOOL            emptyLine;
    WORD            flags;
} TEXTFILE;

/*
// TEXTFILE flags
*/
#define TEXTFILE_FLAG_NONE          (0)
#define TEXTFILE_FLAG_NULL_FILE     (0x0001)
#define TEXTFILE_FLAG_FORCE_CR      (0x0002)

/*
// text file functions
*/
BOOL OpenFile(const char *filename, const char *openFlags, TEXTFILE *textFile);
BOOL OpenFD(int filedes, const char *openFlags, TEXTFILE *textFile);
void CloseFile(TEXTFILE *textFile);

void CreateNullFile(TEXTFILE *textFile);

BOOL GetFileChar(TEXTFILE *textFile, char *ch);
BOOL PutFileString(TEXTFILE *textFile, const char *ch);

#endif

