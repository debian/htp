/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
//
// defs.h
//
// major definitions
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef DEFS_H
#define DEFS_H

#include "htp.h"

/*
// default response filename
*/
extern const char *DEFAULT_RESPONSE_FILE;

/*
// variable types
*/
#define VAR_TYPE_SET_MACRO      (1)
#define VAR_TYPE_BLOCK_MACRO    (2)
#define VAR_TYPE_INTERNAL       (3)
#define VAR_TYPE_ALTTEXT        (4)
#define VAR_TYPE_DEF_MACRO      (5)
#define VAR_TYPE_BLOCKDEF_MACRO (6)

/*
// variable flags
*/
#define VAR_FLAG_NONE           (0x0000)
#define VAR_FLAG_DEALLOC_VALUE  (0x0002)
#define VAR_FLAG_DEALLOC_NAME   (0x0004)
#define VAR_FLAG_UNDEFINED      (0x0008)

/*
// specialized markup processors type definitions
*/

/* MARKUP_FUNC return codes */
#define MARKUP_OKAY             (0)
#define MARKUP_REPLACED         (1)
#define DISCARD_MARKUP          (2)
#define MARKUP_ERROR            ((uint) -1)
#define NEW_MARKUP              (3)

/*
// when reading source files, need to dynamically allocate memory to avoid
// overruns ... use a stronger strategy for "real" operating systems, more
// conservative for wimpy DOS
*/
#if __MSDOS__

#define MIN_PLAINTEXT_SIZE      (128)
#define PLAINTEXT_GROW_SIZE     (32)

#else

#define MIN_PLAINTEXT_SIZE      (2048)
#define PLAINTEXT_GROW_SIZE     (2048)

#endif


/*
// miscellaneous definitions
*/
#define MAX_TIME_DATE_SIZE      (128)
#define DEFAULT_PRECISION       (0)
#define SEARCH_PATH_SIZE        (1024)
#define MAX_CONDITIONAL_LEVEL   (32)

/*
// htp task structure
//
// (the word "task" is not to be confused with the traditional operating system
// term ... it is used here to represent all the information associated with
// the particular job at hand, which is reading in a file, writing out to
// a file, and maintaining information during the entire operation)
*/
typedef struct tagTASK
{
    STREAM          *infile;
    STREAM          *outfile;
    VARSTORE        *varstore;
    const char      *sourceFilename;
    uint            conditionalLevel;
    uint            conditionalLine[MAX_CONDITIONAL_LEVEL];
} TASK;

/*
// markup processor function and array association structure
*/
typedef uint (*MARKUP_FUNC)(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);
typedef struct tagMARKUP_PROCESSORS
{
    const char      *tag;
    uint            markupType;
    MARKUP_FUNC     markupFunc;
} MARKUP_PROCESSORS;

/*
// template file name (used internally to store name for post-processing)
// use squirrelly characters and a space to avoid conflicting with
// user names
*/
extern const char *VAR_TEMPLATE_NAME;

/*
// forward references
*/
BOOL ProcessTask(TASK *task);
BOOL OptionCallback(const char *name, const char *value, ulong userParam);

/*
// the user can configure what kind of characters to use to surround htp
// markups, to avoid conflicts with HTML markups ... default is the standard
// greater-than/less-than bracketing, but also acceptable are square
// brackets and curly brackets (parentheses are just too common in normal
// text to be useful)
//
// Because htp also processes standard HTML markups, a IS_OPEN_MARKUP and
// IS_CLOSE_MARKUP macros are used instead of standard comparisons ... watch
// out for side-effects
//
// MARKUP_TYPE_ANY is used for markup processors to define they are
// interested in either kind of markup (currently unused)
//
// MARKUP_OPEN_DELIM and MARKUP_CLOSE_DELIM are used to return the proper
// delimiter given the markup type
*/
#define HTML_OPEN_MARKUP            ('<')
#define HTML_CLOSE_MARKUP           ('>')

extern char htpOpenMarkup;
extern char htpCloseMarkup;

#define IS_OPEN_MARKUP(c)           (((c) == '<') || ((c) == htpOpenMarkup))
#define IS_CLOSE_MARKUP(c)          (((c) == '>') || ((c) == htpCloseMarkup))

#define MARKUP_TYPE_HTML            (0x0001)
#define MARKUP_TYPE_HTP             (0x0002)
#define MARKUP_TYPE_ANY             (0xFFFF)

#define MARKUP_OPEN_DELIM(t) \
    (((t) & MARKUP_TYPE_HTP) ? htpOpenMarkup : HTML_OPEN_MARKUP)

#define MARKUP_CLOSE_DELIM(t) \
    (((t) & MARKUP_TYPE_HTP) ? htpCloseMarkup : HTML_CLOSE_MARKUP)

/*
// include file search path
*/
extern char searchPath[SEARCH_PATH_SIZE];

#endif
