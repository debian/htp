/*
//
// ver.h
//
// version and program information
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef VER_H
#define VER_H

/*
// program version number
*/
#include "version.inc"

void DisplayHeader(void);
void usage(void);

#endif

