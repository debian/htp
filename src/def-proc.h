/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
//
// def-proc.h
//
// specialized markup processors
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef DEF_PROC_H
#define DEF_PROC_H

#include "defs.h"

/* Reads in a block and puts it into a temporary file.  The name of
 * the temporary file is put into newFilename, which must be able to hold
 * MAX_PATHNAME_LEN characters.
 */
void BlockDestructor(const char *name, void *value, 
                     uint type, uint flags, void *param);
BOOL ReadBlockToFile(TASK *task, BOOL expand, 
                     const char *tag, STREAM *blockStream);
BOOL ReadinBlock(TASK *task, HTML_MARKUP *htmlMarkup, STREAM *blockStream);

uint BlockProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);

uint UndefProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);


#endif
