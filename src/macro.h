/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
//
// macro.h
//
// specialized markup processors
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef MACRO_H
#define MACRO_H

#include "defs.h"

BOOL ExpandMacrosInString(TASK *task, const char *text, char **newText,
                          int *quoted, uint *changeCount);


BOOL ExpandMacros(TASK *task, HTML_MARKUP *htmlMarkup);

#define METATAG_MAX_OPTIONS                     (256)

uint ExpandMetatag(TASK *task, HTML_MARKUP *htmlMarkup);

uint ExpandAll(TASK *task, HTML_MARKUP *htmlMarkup, 
               char** newPlaintextPtr, uint markupType);


#endif
