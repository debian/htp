/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
//
// -proc.h
//
// specialized markup processors
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef FILE_PROC_H
#define FILE_PROC_H

#include "defs.h"

/*
uint ExternalFileProcessor(TASK *task, HTML_MARKUP *htmlMarkup,
                           const char *externalName, char **newPlaintext);

uint FileExecuteProcessor(TASK *task, HTML_MARKUP *htmlMarkup);

uint FileTemplateProcessor(TASK *task, HTML_MARKUP *htmlMarkup);

uint FileIncludeProcessor(TASK *task, HTML_MARKUP *htmlMarkup);
*/

uint FileProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);
uint OutputProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);

#endif
