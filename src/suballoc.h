/*
//
// suballoc.h
//
// Memory suballocation ... rather than go out to heap for all the memory,
// htp maintains a small cache of recently allocated memory for speedier
// requests
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef SUBALLOC_H
#define SUBALLOC_H

/*
//#undef USE_SUBALLOC
*/

#ifndef USE_SUBALLOC

#define AllocMemory(size) malloc(size)
#define FreeMemory(ptr) free(ptr)
#define ResizeMemory(ptr, newSize)  realloc(ptr, newSize)

#else

/*
// performance counters
*/
#if DEBUG
extern uint totalAllocations;
extern uint freePoolHits;
#endif


/*
// suballoc has several features that can be controlled via #defines
// most features are turned off for release versions of the software to
// speed up execution
//
// These features are:
//
//      SUBALLOC_WARNING        prints warning to stdout when problems are
//                              detected
//
//      SUBALLOC_CLEARMEM       all memory is cleared with a special bit
//                              pattern (not all zero or all one) when
//                              allocated and freed
//
//      SUBALLOC_DEBLOG         will print all allocs and frees to a debug
//                              log
//
//      SUBALLOC_MINALLOCSIZE   size (in bytes) of minimum allocation ...
//                              helps prevent heap thrashing when lots of
//                              small allocations occur ... if set to 0
//                              then allocation sizes match requested size
//
//      SUBALLOC_FIRSTFIT       if set, suballoc pulls the first properly sized
//                              block out of its free pool, rather than the
//                              one with the best fit ... faster, but may lead
//                              to unnecessary allocs from the system heap
//
//      SUBALLOC_MAXFREEPOOLSIZE if set, suballoc will keep the free pool
//                              size equal to or less than this amount
//                              ... too small a size will lead to excessive
//                              heap allocations, too large could cause the
//                              program to keep vital memory from the operating
//                              system
//
*/

#define SUBALLOC_MINALLOCSIZE       (64)
#define SUBALLOC_FIRSTFIT           (1)

#if DEBUG

#define SUBALLOC_WARNING            (1)
#define SUBALLOC_CLEARMEM           (1)
#define SUBALLOC_DEBLOG             (0)

#else

#define SUBALLOC_WARNING            (0)
#define SUBALLOC_CLEARMEM           (0)
#define SUBALLOC_DEBLOG             (0)

#endif

#if __MSDOS__

#define SUBALLOC_MAXFREEPOOLSIZE    (60 * KBYTE)

#else

#define SUBALLOC_MAXFREEPOOLSIZE    (512 * KBYTE)

#endif

/*
// Initialize and terminate functions ... should be called before and
// after using following functions
*/
void InitializeMemory(void);
void TerminateMemory(void);

/*
// AllocMemory
//
// malloc() look-alike
*/
void *_AllocMemory(uint size, const char *file, uint line);
#define AllocMemory(size)       _AllocMemory(size, __FILE__, __LINE__)

/*
// FreeMemory
//
// free() look-alike
*/
void _FreeMemory(void *ptr, const char *file, uint line);
#define FreeMemory(ptr)         _FreeMemory(ptr, __FILE__, __LINE__);

/*
// ResizeMemory
//
// realloc() look-alike
*/
void *_ResizeMemory(void *ptr, uint newSize, const char *file, uint line);
#define ResizeMemory(ptr, newSize) \
    _ResizeMemory(ptr, newSize, __FILE__, __LINE__)

/*
// MemorySize
//
// Returns available size of buffer
*/
uint MemorySize(void *ptr);
#endif

#endif

