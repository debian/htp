/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
//
// use-proc.h
//
// specialized markup processors
//
// Copyright (c) 1995-96 Jim Nelson.  Permission to distribute
// granted by the author.  No warranties are made on the fitness of this
// source code.
//
*/

#ifndef USE_PROC_H
#define USE_PROC_H

#include "defs.h"


uint UseProcessor(TASK *task, HTML_MARKUP *htmlMarkup, char **newPlaintext);


#endif
